import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {BackendRoutes} from '../../routing/backend';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.css']
})
export class AuthPageComponent implements OnInit {
  constructor(
      private router: Router
  ) { }

  ngOnInit() {}
  redirectToAuth(): void {
      window.location.href = BackendRoutes.AuthRoute;
  }
}
