import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {BackendRoutes} from '../../routing/backend';
import {VkCabinetsService} from '../../services/vk-cabinets/vk-cabinets.service';
import {Subscription} from 'rxjs';
import {Cabinet} from '../../models/cabinet';

@Component({
  selector: 'app-cabinets-page',
  templateUrl: './cabinets-page.component.html',
  styleUrls: ['./cabinets-page.component.css']
})
export class CabinetsPageComponent implements OnInit, OnDestroy, AfterViewInit {
  private _subscription: Subscription;
  private _cabinets: Array<Cabinet>;

  constructor(
      public userService: UserService,
      public cabinetsService: VkCabinetsService
  ) {
    this._subscription = this.cabinetsService.cabinetsUpdatedObservable.subscribe((cabinets: Array<Cabinet>) => {
      this._cabinets = cabinets;
    });
  }

  ngOnInit() {}
  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
  ngAfterViewInit() {
      this.cabinetsService.getCabinets();
  }
  logout(): void {
      window.location.href = BackendRoutes.AuthLogout;
  }

  get cabinets(): Array<Cabinet> { return this._cabinets; }
}
