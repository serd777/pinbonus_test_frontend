import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinetsPageComponent } from './cabinets-page.component';

describe('CabinetsPageComponent', () => {
  let component: CabinetsPageComponent;
  let fixture: ComponentFixture<CabinetsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinetsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinetsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
