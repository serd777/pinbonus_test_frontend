import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {UserService} from '../../services/user/user.service';

@Injectable({
  providedIn: 'root'
})
export class GuestGuard implements CanActivate {
    private _subscription: Subscription;

    constructor(
        private userService: UserService,
        private router: Router
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return new Promise<boolean>((resolve, reject) => {
            this._subscription = this.userService.initedObservable.subscribe(() => {
                if (this.userService.isAuth) {
                    resolve(true);
                    this.router.navigate(['/cabinets']);
                }

                resolve(true);
                this._subscription.unsubscribe();
            });
        });
    }
}
