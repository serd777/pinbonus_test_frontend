
export const BackendHost = 'http://pinbonus.localhost/backend/';

export const BackendRoutes = {
    AuthRoute: BackendHost + 'auth/vk',
    AuthLogout: BackendHost + 'auth/logout',

    UserInfo: BackendHost + 'user/info',

    GetAdsCabinets: BackendHost + 'adv/get_cabinets'
};
