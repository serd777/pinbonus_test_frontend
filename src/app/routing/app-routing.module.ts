import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../guards/auth/auth.guard';
import {AuthPageComponent} from '../pages/auth-page/auth-page.component';
import {CabinetsPageComponent} from '../pages/cabinets-page/cabinets-page.component';
import {GuestGuard} from '../guards/guest/guest.guard';

const routes: Routes = [
    {path: '', component: AuthPageComponent, canActivate: [GuestGuard]},
    {path: 'cabinets', component: CabinetsPageComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
