import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

export interface ServerAnswer {
    status: boolean;
    data: any;
}

@Injectable({
  providedIn: 'root'
})
export class RequestsService {
    private options: object;

    constructor(
        public http: HttpClient
    ) {
        this.options = {withCredentials: true};
    }

    //  Post Request
    public postRequest(url: string, data: object = {}, callback = ((answer) => {})) {
        this.http.post<ServerAnswer>(url, data, this.options)
            .subscribe(callback);
    }

    //  Get Request
    public getRequest(url: string, callback = ((answer) => {})) {
        this.http.get<ServerAnswer>(url, this.options)
            .subscribe(callback);
    }
}
