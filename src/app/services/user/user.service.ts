import { Injectable } from '@angular/core';
import {RequestsService, ServerAnswer} from '../requests/requests.service';
import {Observable, Subject} from 'rxjs';
import {BackendRoutes} from '../../routing/backend';
import {User} from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _isAuth: boolean;
  private _initedSubject: Subject<boolean>;
  private _user: User;

  constructor(
      private requestsService: RequestsService
  ) {
    this._isAuth = false;
    this._initedSubject = new Subject<boolean>();
    this._user = new User();

    this.initService();
  }

  //  Метод инициализации
  private initService(): void {
    this.requestsService.getRequest(
        BackendRoutes.UserInfo,
        (answer: ServerAnswer) => {
          if (answer.status) {
              this._user = answer.data as User;
          }

          this._isAuth = answer.status;
          this._initedSubject.next(true);
        }
    );
  }

  get initedObservable(): Observable<boolean> { return this._initedSubject.asObservable(); }
  get isAuth(): boolean { return this._isAuth; }
  get user(): User { return this._user; }
}
