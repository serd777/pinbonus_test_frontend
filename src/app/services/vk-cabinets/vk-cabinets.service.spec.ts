import { TestBed } from '@angular/core/testing';

import { VkCabinetsService } from './vk-cabinets.service';

describe('VkCabinetsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VkCabinetsService = TestBed.get(VkCabinetsService);
    expect(service).toBeTruthy();
  });
});
