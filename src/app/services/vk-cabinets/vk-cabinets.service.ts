import { Injectable } from '@angular/core';
import {RequestsService, ServerAnswer} from '../requests/requests.service';
import {Cabinet} from '../../models/cabinet';
import {BackendRoutes} from '../../routing/backend';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VkCabinetsService {
  private _cabinetsUpdated: Subject<Array<Cabinet>>;

  constructor(
      private requestsService: RequestsService
  ) {
    this._cabinetsUpdated = new Subject<Array<Cabinet>>();
  }

  public getCabinets(): void {
    this.requestsService.getRequest(
        BackendRoutes.GetAdsCabinets,
        (answer: ServerAnswer) => {
          if (answer.status) {
            this._cabinetsUpdated.next(answer.data as Array<Cabinet>);
          }
        }
    );
  }

  get cabinetsUpdatedObservable(): Observable<Array<Cabinet>> { return this._cabinetsUpdated.asObservable(); }
}
