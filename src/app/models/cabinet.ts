
export class Cabinet {
    constructor(
        public id: number = 0,
        public remote_id: number = 0,
        public remote_type: string = '',
        public remote_status: boolean = false,
        public remote_role: string = ''
    ) {}
}
